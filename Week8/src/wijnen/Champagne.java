package wijnen;

import java.time.LocalDate;

/**
 * PEER opdracht
 * P2W2
 */
public class Champagne extends Wijn {
    private Smaak smaak;


    public Champagne(String naam, String streek, LocalDate oogstDatum, double basisPrijs,Smaak smaak) {
        super(naam, streek, oogstDatum, basisPrijs);
        this.smaak = smaak;
    }

    @Override
    public double berekenPrijs() {

        if(smaak.toString().toLowerCase().contains("brut")) {
            return this.getBasisPrijs()*1.1;
        }

        return super.berekenPrijs();
    }

    @Override
    public String toString() {
        return String.format("%-45s € %5.2f\n\t(%s) --> Type: %s",this.getNaam(),berekenPrijs(),getKenmerken(),this.smaak);

    }
}
