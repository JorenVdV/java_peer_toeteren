package wijnen;

import java.time.LocalDate;

/**
 * PEER opdracht
 * P2W2
 */
public class Likeur extends Wijn {
    private double alcoholGehalte; //in procent


    public Likeur(String naam, String streek, LocalDate oogstDatum, double basisPrijs, double alcoholGehalte) {
        super(naam, streek, oogstDatum, basisPrijs);
        this.alcoholGehalte = alcoholGehalte;
    }

    /**
     * 2.2. Doe een override van de methode berekenPrijs: Als het alcoholgehalte van de likeur 50% of
     * meer bedraagt, dan komt er een kwart bij de basisprijs bij.
     */

    @Override
    public double berekenPrijs() {
        if (this.alcoholGehalte >= 0.5) {
            return this.getBasisPrijs()*1.25;
        }

        return this.getBasisPrijs();
    }

    /**
     * 2.3. Doe een override van de methode toString. Genereer een string met onderstaande lay-out.
     * Maak daarbij gebruik van de toString van de superklasse.
     */

    @Override
    public String toString() {
        return String.format("%-45s € %5.2f\n\t(%s) --> %2.0f%% alc",this.getNaam(),berekenPrijs(),getKenmerken(),this.alcoholGehalte*100);
    }
}
