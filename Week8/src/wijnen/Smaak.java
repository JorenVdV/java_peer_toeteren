package wijnen;

/**
 * PEER opdracht
 * P2W2
 */
public enum Smaak {

    /**
     * Pas de toString methode van Smaak aan zodat de enum-waarden als volgt worden
     * weergegeven: "Brut", "Extra-brut", "Brut sans Millésime", "Sec", "Demi-sec", "Doux"
     */
    BRUT("Brut"), EXTRA_BRUT("Extra-brut"), BRUT_SANS_MILLESIME("Brut sans Millésime"), SEC("Sec"), DEMI_SEC("Demi-sec"), DOUX("Doux");


    private String naam;

    Smaak(String naam) {
        this.naam = naam;
    }

    @Override
    public String toString() {
        return naam;
    }
}
