package wijnen;

import java.time.LocalDate;

/**
 * PEER opdracht
 * P2W2
 */
public class WijnHuis {
    private static final int MAX_AANTAL = 10;
    private Wijn[] wijnen = new Wijn[MAX_AANTAL];  //voorlopig gevuld met 10 null-objecten
    private String naam;
    private int aantal = 0;

    public WijnHuis(String naam) {
        this.naam = naam;
    }


    /**
     * 5.2. Werk de methode voegWijnToe verder uit. Voeg de nieuwe wijn toe, maar enkel als er nog plaats
     * is in de array EN als de wijn nog niet in de array voorkomt. TIP: voor dit laatste roep je de methode
     * zoekWijn op. Vergeet ook niet de teller aantal te verhogen!
     */
    public void voegWijnToe(Wijn wijn) {

        if (this.aantal < MAX_AANTAL && !this.zoekWijn(wijn)) {
            wijnen[this.aantal++] = wijn;
        }

    }

    /**
     *
     * 5.1. Werk de methode zoekWijn verder uit. Gebruik een for-each lus om de array van wijnen te
     * overlopen en retourneer een boolean als je de naam van de wijn al dan niet gevonden hebt.
     * Opgelet: strings vergelijken doe je NIET met ==
     */
    public boolean zoekWijn(Wijn wijn) {


        for (Wijn w : wijnen) {
            if (w != null) {


                if (w.getNaam().equals(wijn.getNaam())) {
                    return true;
                }
            }
        }

        return false;

    }

    /**
     * 5.3. Werk de methode getOudsteWijn uit. Ga daarin op zoek naar de oudste wijn van het huis.
     * Controleer eerst of de array niet leeg is; dan retourneer je null.
     */
    public Wijn getOudsteWijn() {

        if (this.wijnen.length == 0) {
            return null;
        }

        Wijn oudste = this.wijnen[0];

        for (Wijn wijn : wijnen) {
            if (wijn.getOogstDatum().isBefore(oudste.getOogstDatum())) {
                oudste = wijn;
            }
        }

        return oudste;
    }


    /**
     * Werk de methode toString verder uit. Genereer een overzicht in de vorm van een string die alle
     * wijnsoorten van het wijnhuis presenteert. Eerst alle gewone wijnen, daarna de Champagnes en
     * tenslotte de likeuren. Gebruik de StringBuilder-objecten die er al staan. Eigenlijk heb je maar
     * één lus nodig!
     * TIP: gebruik instanceof om te testen of je met een likeur of een Champagne te maken hebt.
     */

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(String.format("Wijnhuis %s\n", naam.toUpperCase()));

        StringBuilder wijnenTekst = new StringBuilder();
        StringBuilder champagneTekst = new StringBuilder();
        StringBuilder likeurenTekst = new StringBuilder();

        for (Wijn wijn : wijnen) {
            if (wijn instanceof Champagne) {
                champagneTekst.append(wijn.toString()+"\n");
            }
            else if (wijn instanceof  Likeur) {
                likeurenTekst.append(wijn.toString()+"\n");
            }
            else {
                wijnenTekst.append(wijn.toString()+"\n");
            }
        }

        result.append("Wijnen:\n");
        result.append(wijnenTekst);
        result.append("Champagnes:\n");
        result.append(champagneTekst);
        result.append("Likeuren:\n");
        result.append(likeurenTekst);

        return result.toString();
    }
}
