package todo;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

/**
 * Peer Tutoring
 * P2W4 - Exceptions
 */
public class TodoItem {

    private String titel;
    private LocalDate deadline;
    private boolean afgewerkt;

    public TodoItem(String titel, LocalDate deadline) {

        if (titel == null || titel.length() == 0) {
            throw new IllegalArgumentException("De titel is leeg");
        }
        this.titel = titel;
        this.setDeadline(deadline);
        this.afgewerkt = false;
    }

    public void setDeadline(LocalDate deadline) {

        if (deadline.isBefore(LocalDate.now())) {
            throw new IllegalArgumentException("De deadline ligt in het verleden.");
        }
        this.deadline = deadline;
    }

    public boolean isAfgewerkt() {
        return afgewerkt;
    }

    public void setAfgewerkt(boolean afgewerkt) {
        this.afgewerkt = afgewerkt;
    }

    @Override
    public String toString() {

        String l;

        Period diff = LocalDate.now().until(this.deadline);

        if (!this.afgewerkt) {
            l = String.format("Nog %d jaar, %d maand(en) %d dag(en)",diff.getYears(),diff.getMonths(),diff.getDays());
        }
        else {
            l = "Klaar!";
        }

        return String.format("%-30s %-15s %-35s", this.titel,this.deadline.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")), l);
    }
}

