package todo;

/**
 * Peer Tutoring
 * P2W4 - Exceptions
 */
public class TodoLijstException extends Exception{
    //Opgave 2

    public TodoLijstException(String message) {
        super(message);
    }
}
