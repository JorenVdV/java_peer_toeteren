package todo;

import java.time.LocalDate;

/**
 * Peer Tutoring
 * P2W4 - Exceptions
 */
public class TodoLijst {
    private static final int MAX_AANTAL = 20;
    private TodoItem[] items;
    private int aantalItemsInLijst;

    public TodoLijst() {
        this.items = new TodoItem[MAX_AANTAL];
        this.aantalItemsInLijst = 0;
    }

    public void add(TodoItem newItem) throws TodoLijstException{
        //Opgave 3.1
        if (aantalItemsInLijst >= MAX_AANTAL) {
            throw new TodoLijstException("Maximum bereikt...");
        }

        items[aantalItemsInLijst++] = newItem;

    }

    public void zetOpAfgewerkt(int index) throws TodoLijstException {
        //Opgave 3.2
       if (index < 0 || index > aantalItemsInLijst) {
           throw new TodoLijstException("Index is niet juist");
       }

       items[index].setAfgewerkt(true);
    }

    public void printAll() {
        //Opgave 3.3
        for (TodoItem item : items) {
            if (item != null) {
                System.out.println(item);
            }
        }
    }

    public void printNietAfgewerkt() {
        //Opgave 3.4
        for (int i = 0; i < aantalItemsInLijst; i++) {
            if (items[i] != null && !items[i].isAfgewerkt()) {
                System.out.println(String.format("%s %s",i+")", items[i]));
            }
        }
    }
}