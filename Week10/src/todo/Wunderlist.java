package todo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Scanner;

/**
 * Peer Tutoring
 * P2W4 - Exceptions
 */
public class Wunderlist {
    public void leesBestand(String fileName) {
        TodoLijst todoLijst = new TodoLijst();
        Path myFile = Paths.get(fileName);
        Scanner scanner = null;

        //Opgave 4
        if (!Files.exists(myFile)) {
            System.out.println("Bestand bestaat niet!");
        } else {
            try {
                scanner = new Scanner(myFile);

                while (scanner.hasNext()) {
                    String[] s = scanner.nextLine().split(",");

                    TodoItem item = new TodoItem(s[0], LocalDate.parse(s[1]));

                    if (s[2].equals("J")) {
                        item.setAfgewerkt(true);
                    }

                    todoLijst.add(item);
                }

                scanner.close();
            } catch (Exception e) {
                System.out.println("Beschadigd bestand!\n" + e.getMessage());
            } finally {
                if (scanner != null) {
                    scanner.close();
                }
            }
        }

        System.out.printf("De inhoud van %s:\n", fileName);
        System.out.println("----------------------------------------");
        todoLijst.printAll();
        System.out.println("----------------------------------------");


    }
}
