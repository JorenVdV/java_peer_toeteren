package Deel1;

import java.util.Scanner;

/**
 * Created by brent on 3-10-2016.
 */


public class Bloedgeven {

    private static final double MIN_DONATIE = 0.45;

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        char keuze;

        //TATTOO
        System.out.print("Heb je de laatste 4 maanden een tatoeage laten zetten? (J/N): ");
        keuze = keyboard.nextLine().charAt(0);

        if (keuze == 'J') {
            System.out.println("Jammer, je komt niet in aanmerking om bloed te geven.");
            System.exit(0);
        }

        //GESLACHT
        char geslacht = 'T';

        while (geslacht != 'M' && geslacht != 'V') {
            System.out.println("Ben je een man of vrouw (M/V):");
            geslacht = keyboard.nextLine().charAt(0);
        }

        if (geslacht == 'M') {
            System.out.println("Heb je seksuele betrekkingen gehad met een andere man? (J/N):");

            keuze = keyboard.nextLine().charAt(0);

            if (keuze == 'J') {
                System.out.println("Jammer, je komt niet in aanmerking om bloed te geven.");
                System.exit(0);
            }
        } else {
            System.out.println("Ben je zwanger? (J/N):");

            keuze = keyboard.nextLine().charAt(0);

            if (keuze == 'J') {
                System.out.println("Jammer, je komt niet in aanmerking om bloed te geven.");
                System.exit(0);
            }
        }

        //LEEFTIJD
        System.out.print("Wat is je leeftijd?");
        int leeftijd = keyboard.nextInt();

        if (leeftijd < 18) {
            System.out.println("Jammer, je komt niet in aanmerking om bloed te geven.");
            System.exit(0);
        } else if (leeftijd > 66) {

            System.out.println("Hoeveel jaren geleden heb je voor 't laatst bloed gegeven (0 voor nooit):");
            int jaren = keyboard.nextInt();

            if (jaren == 0) {
                System.out.println("Jammer, je komt niet in aanmerking om bloed te geven.");
                System.exit(0);
            } else if (jaren > 3 || leeftijd > 71) {
                System.out.println("Jammer, je komt niet in aanmerking om bloed te geven.");
                System.exit(0);
            }

        }

        //LENGTE
        System.out.println("Wat is je lengte (in m):");
        double lengte = keyboard.nextDouble();
        System.out.println("Wat is je gewicht (in kg):");
        int gewicht = keyboard.nextInt();


        //OUTPUT
        double bloedvolume;
        double max_donatie;
        boolean mag_bloed_geven = true;

        if (geslacht == 'M') {
            bloedvolume = (0.3669 * Math.pow(lengte, 3)) + (0.03219 * gewicht + 0.6041);
        } else {
            bloedvolume = (0.3561 * Math.pow(lengte, 3)) + (0.03308 * gewicht + 0.1833);
        }

        max_donatie = bloedvolume * 13 / 100;

        if (max_donatie < MIN_DONATIE) {
            mag_bloed_geven = false;
        }

        System.out.println("\nBloedvolume: " + bloedvolume);
        System.out.println("Max donatie: " + max_donatie);
        System.out.println("Min donatie: " + MIN_DONATIE);

        if (mag_bloed_geven) {
            System.out.println("Je mag WEL bloed geven");
        } else {
            System.out.println("Je mag GEEN bloed geven");
        }

        /**
         * UITBREIDING
         */

        String strBloedVolume = "";
        for (double i = 0.1; i <= bloedvolume; i += 0.1) {
            strBloedVolume += "*";
        }

        String strMaxDonatie = "";
        for (double i = 0.1; i <= max_donatie; i += 0.1) {
            strMaxDonatie += "*";
        }

        String strMinDonatie = "";
        for (double i = 0.1; i <= MIN_DONATIE; i += 0.1) {
            strMinDonatie += "*";
        }

        System.out.printf("\nBloedvolume: %.2f %s\n",bloedvolume,strBloedVolume);
        System.out.printf("Max donatie: %.2f %s\n",max_donatie,strMaxDonatie);
        System.out.printf("Min donatie: %.2f %s",MIN_DONATIE,strMinDonatie);



    }
}
