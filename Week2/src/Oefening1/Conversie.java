package Oefening1;

import java.util.Scanner;

/**
 * Created by brent on 26/09/2016.
 */
public class Conversie {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int keuze = 1;
        int input;
        float conversie;

        System.out.println("Conversie graden Celsius - Fahrenheit" +
                "\n=====================================");

        while (true) {

            System.out.print("Welke conversie wens je te doen?" +
                    "\n\t1) °C naar °F" +
                    "\n\t2) °F naar °C" +
                    "\nUw keuze?");

            keuze = scanner.nextInt();

            /*
             * CONVERSIE VAN CELSIUS NAAR FAHRENHEIT
             */
            if (keuze == 1) {
                System.out.println("Geef de waarde in °C:");
                input = scanner.nextInt();

                conversie = input * 9 / 5 + 32;

                System.out.print(input + ".0°C = " + conversie + "°F\n");

            }

            /*
             * CONVERSIE VAN FAHRENHEIT NAAR CELSIUS
             */
             if (keuze == 2) {

                System.out.println("Geef de waarde in °F:");
                input = scanner.nextInt();

                conversie = (input - 32) * 5 / 9;

                System.out.print(input + ".0°F = " + conversie + "°C\n");
            }

            if (keuze == 0){
                return;
            }
        }
    }
}
