package Oefening2;

import java.util.Scanner;

/**
 * Created by brent on 26/09/2016.
 */
public class Tabel {

    public static void main(String[] args) {

        int beginTemp;
        int eindTemp;
        int stap;
        float fahrenheit;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Conversietabel °C naar °F\n" +
                "=========================\n");
        System.out.println("Geef de begintemperatuur in °C:");
        beginTemp = scanner.nextInt();

        System.out.println("Geef de eindtemperatuur in °C:");
        eindTemp = scanner.nextInt();

        /*
         * EXTRA STAP (2)
         */
        if (beginTemp > eindTemp) {
            System.out.println("De begintemperatuur moet kleiner zijn dan de eindtemperatuur!");
            return;
        }

        System.out.println("Geef de stapwaarde:");
        stap = scanner.nextInt();

/*
        while (beginTemp <= eindTemp) {

            fahrenheit = beginTemp * 9 / 5 + 32;

            System.out.println(beginTemp + "°C = " + fahrenheit + "°F");

            beginTemp += stap;
        }
*/

        //UITDAGING


        System.out.println("\n===============");
        System.out.printf( "|  °C  |  °F  |");
        System.out.println("\n|-------------|");

        while (beginTemp <= eindTemp) {

            fahrenheit = beginTemp * 9 / 5 + 32;

            System.out.printf("| %4d | %4.1f |\n", beginTemp, fahrenheit);

            beginTemp += stap;


        }
        System.out.println("===============");


    }
}