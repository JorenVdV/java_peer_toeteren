import java.util.Scanner;

/**
 * Created by brent on 10/10/2016.
 */
public class Galgje3 {

    public static void main(String[] args) {

        final String HANGMAN =
                "----- \n" +
                        "|   | \n" +
                        "|   O \n" +
                        "|  /|\\\n" +
                        "|   | \n" +
                        "|  / \\\n" +
                        "|     \n" +
                        "-------";

        System.out.println(HANGMAN.length());

        String woord = "";
        Scanner keyboard = new Scanner(System.in);

        while (woord.length() > 10 || woord.length() < 5) {
            System.out.println("Geef een woord (max 10 letters): ");
            woord = keyboard.nextLine();

        }

        for (int i = 0; i < 20; i++) {
            System.out.println();
        }

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < woord.length(); i++) {
            builder.append(".");
        }


        char gok = 'M';
        for (int i = 1; i < 9; i++) {
            System.out.printf("Het te zoeken woord: %s\n", builder.toString());
            System.out.println("Raad een letter: ");

            gok = keyboard.next().charAt(0);

            for (int j = 0; j < woord.length(); j++) {
                if (woord.charAt(j) == gok) {
                    builder.setCharAt(j, gok);
                }
            }



            if (builder.toString().equals(woord)) {
                System.out.printf("Proficiat je hebt het woord geraden in %d beurten!", i);
                break;
            }

            System.out.printf(HANGMAN.substring(0,i*7));

        }

    }
}
