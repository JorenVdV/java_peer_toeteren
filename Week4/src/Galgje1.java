import java.util.Scanner;

/**
 * Created by brent on 10/10/2016.
 */
public class Galgje1 {

    public static void main(String[] args) {

        String woord = "";
        Scanner keyboard = new Scanner(System.in);

        while(woord.length() > 10 || woord.length() < 5) {
            System.out.println("Geef een woord (max 10 letters): ");
            woord = keyboard.nextLine();

        }

        for (int i = 0; i < 20; i++) {
            System.out.println();
        }

        String puntjes = "";
        for (int i = 0; i < woord.length(); i++) {
            puntjes += ".";
        }

        System.out.printf("Het te zoeken woord: %s\n",puntjes);



        for (int i = 1; i < 6; i++) {
            System.out.println("Doe een gok: ");

            if (keyboard.nextLine().equals(woord)) {
                System.out.printf("Proficiat je hebt het woord geraden in %d beurten!",i);
                break;
            }
            System.out.println("Fout!");

        }

    }
}
