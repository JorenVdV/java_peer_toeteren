package Domino;

/**
 * Peer Tutoring
 * P2W5 - Collections
 */
public class DominoSteen {
    private int getal1;
    private int getal2;

    public DominoSteen(int getal1, int getal2) {
        this.getal1 = getal1;
        this.getal2 = getal2;
    }

    public int getGetal1() {
        return getal1;
    }

    public int getGetal2() {
        return getal2;
    }

    public int getAantalPunten(){
        int punten = 0;
        //OPGAVE 1
        return getal1==getal2?getal1*4:getal1+getal2;
    }

    public void verwisselGetallen(){
        //OPGAVE 2
        int temp = getal1;
        getal1 = getal2;
        getal2 = temp;
    }

    public String toString(){
        return String.format("{%d, %d}", getal1, getal2);
    }
}
