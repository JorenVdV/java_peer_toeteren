package Domino;

import java.util.*;

/**
 * Peer Tutoring
 * P2W5 - Collections
 */
public class Speler {
    private String naam;
    //OPGAVE 3
    private List<DominoSteen> stenen;


    public Speler(String naam) {
        this.naam = naam;
        //OPGAVE 3
        stenen = new ArrayList<>();
    }

    public String getNaam() {
        return naam;
    }

    public int getAantalStenen(){
        //OPGAVE 4
        return stenen.size();
    }

    public  int getAantalPunten(){
        int totaalAantalPunten = 0;
        //OPGAVE 5
        /*
        for(DominoSteen dominoSteen: stenen){
            totaalAantalPunten += dominoSteen.getAantalPunten();
        }
        return totaalAantalPunten; // */
        return stenen.stream().mapToInt(steen -> steen.getAantalPunten()).sum();
    }

    public void neemNieuweSteen(DominoSteen getrokkenSteen){
        //OPGAVE 6
        stenen.add(getrokkenSteen);
        Collections.sort(stenen, Comparator.comparingInt(DominoSteen::getAantalPunten));
    }

    public  DominoSteen zoekSteen(){
        //OPGAVE 7.1
        DominoSteen teZoekenSteen = stenen.get(0);
        for (DominoSteen dominoSteen: stenen){
            if(teZoekenSteen.getAantalPunten() < dominoSteen.getAantalPunten())
                teZoekenSteen = dominoSteen;
        }
        stenen.remove(teZoekenSteen);
        return teZoekenSteen;
    }

    public DominoSteen zoekSteen(int getal){
        //OPGAVE 7.2
        for (DominoSteen dominoSteen : stenen){
            if(dominoSteen.getGetal1() == getal || dominoSteen.getGetal2() == getal){
                stenen.remove(dominoSteen);
                return dominoSteen;
            }
        }
        return null;
    }


}
