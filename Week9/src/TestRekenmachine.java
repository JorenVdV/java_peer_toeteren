import rekenen.*;
import rekenen.plugins.*;

import java.util.Scanner;

/**
 * PEER TUTORING
 * P2W3
 */
public class TestRekenmachine {
    private static Rekenmachine mijnCalc = new Rekenmachine();

    public static void main(String[] args) {
        //Opgave 3.1

        mijnCalc.installeer(new Optelling());
        mijnCalc.installeer(new Aftrekking());
        mijnCalc.installeer(new Vermenigvuldiging());
        mijnCalc.installeer(new Deling());
        mijnCalc.installeer(new Macht());
        mijnCalc.installeer(new Plugin() {

            @Override
            public String getCommand() {
                return "MIN";
            }

            @Override
            public Double bereken(Double x, Double y) {
                if (x > y) {
                    return y;
                }

                else {
                    return x;
                }
            }

            @Override
            public String getAuteur() {
                return "Anoniem";
            }
        });

        mijnCalc.installeer(new Plugin() {

            @Override
            public String getCommand() {
                return "MAX";
            }

            @Override
            public Double bereken(Double x, Double y) {
                if (x < y) {
                    return y;
                }

                else {
                    return x;
                }
            }

            @Override
            public String getAuteur() {
                return "Anoniem";
            }
        });

        doeBerekeningEnDrukAf("+", 5, 2);
        doeBerekeningEnDrukAf("-", 5, 2);
        doeBerekeningEnDrukAf("*", 5, 2);
        doeBerekeningEnDrukAf("/", 5, 2);
        doeBerekeningEnDrukAf("^", 5, 2);
        doeBerekeningEnDrukAf("?", 5, 2);
        System.out.println(mijnCalc.toString());


        //Opgave 3.2

        Scanner scan = new Scanner(System.in);

        System.out.println("Welkom bij de dynamische rekenmachine!");
        System.out.println(mijnCalc);

        while(true) {
            System.out.println("\nWelke berekening wenst U uit te voeren (<ENTER> om te stoppen)?");

            String berekening = scan.nextLine();

            if (berekening.equals("")) {
                break;
            }

            System.out.println("Geef twee getallen in (gescheiden door een spatie):");

            Double get1 = scan.nextDouble();
            Double get2 = scan.nextDouble();
            scan.nextLine();

            doeBerekeningEnDrukAf(berekening,get1,get2);

        }

        System.out.println("==== LOG ====");
        System.out.println(mijnCalc.getLog());

    }

    //Opgave 3.1
    private static void doeBerekeningEnDrukAf(String commando, double getal1, double getal2){
        System.out.printf("%.2f %s %.2f = %.2f\n"
                , getal1, commando, getal2
                , mijnCalc.bereken(commando, getal1, getal2));
    }


}
