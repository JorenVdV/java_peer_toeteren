package rekenen.plugins;

/**
 * PEER TUTORING
 * P2W3
 */
public interface Plugin {
    //Opgave 1.1

    /*

    een methode getCommand die een String retourneert en géén parameters heeft
    een methode bereken die twee doubles (x en y) als parameters heeft en een double
    retourneert
    een (default) methode getAuteur zonder parameters en als returnwaarde de string “KdG”
     */

    String getCommand();
    Double bereken(Double x, Double y);

    default String getAuteur() {
        return "KdG";
    }


}