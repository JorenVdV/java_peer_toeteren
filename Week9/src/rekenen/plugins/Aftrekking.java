package rekenen.plugins;
/**
 * PEER TUTORING
 * P2W3
 */
public class Aftrekking implements Plugin{
    @Override
    public String getCommand() {
        return "-";
    }

    @Override
    public Double bereken(Double x, Double y) {
        return x-y;
    }
    //Opgave 1.3
}
