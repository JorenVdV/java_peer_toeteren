package rekenen;

import rekenen.plugins.Plugin;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

/**
 * PEER TUTORING
 * P2W3
 */
public class Rekenmachine {
    private final int MAX_AANTAL_PLUGINS = 10;
    private Plugin[] ingeladenPlugins;
    private int aantalPlugins;
    private StringBuilder log;

    public Rekenmachine() {
        this.ingeladenPlugins = new Plugin[MAX_AANTAL_PLUGINS];
        aantalPlugins = 0;
        this.log = new StringBuilder();
    }

    public void installeer(Plugin teInstallerenPlugin) {
        //Opgave 2.1.a

        //Deze methode zal een plugin aan de array ingeladenPlugins toevoegen. Doe hier
        //eerst controle of de nieuwe plugin nog niet voorkomt in de array EN of de array nog niet
        //vol zit. Geef eventueel een gepaste foutmelding.

        if (!Arrays.asList(ingeladenPlugins).contains(teInstallerenPlugin) && aantalPlugins < 10) {
            ingeladenPlugins[aantalPlugins] = teInstallerenPlugin;
            aantalPlugins++;

        }
        else {
            System.out.println("Plugin is al geinstalleerd of er zijn er al 10 geinstalleerd");
        }
    }

    public double bereken(String command, double x, double y) {
        //Opgave 2.1.b

        /*Deze methode onderzoekt eerst of de plugin die bij de operator hoort wel geïnstalleerd
        werd. Is dat niet het geval dan toon je een gepaste foutmelding en je retourneert
        Double.POSITIVE_INFINITY.
                Indien de plugin wel gevonden wordt, dan wordt de methode bereken op deze plugin
        uitgevoerd. Het resultaat van die methode wordt ook teruggestuurd als returnwaarde.
        Indien de plugin niet gevonden wordt, dan geef je het getal “Positief Oneindig” terug*/

        for (Plugin ingeladenPlugin : ingeladenPlugins) {
            if (ingeladenPlugin != null && ingeladenPlugin.getCommand().equals(command)) {

                Double uitkomst = ingeladenPlugin.bereken(x,y);

                //[23 november 2016 14:05:31]
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy HH:mm:ss");
                log.append(String.format("[%s] %f %s %f = %f (by %s)\n",LocalDateTime.now().format(formatter),x,ingeladenPlugin.getCommand(),y,uitkomst,ingeladenPlugin.getAuteur()));


                return uitkomst;
            }
        }

        System.out.println("Plugin niet geinstalleerd");
        return Double.POSITIVE_INFINITY;

    }

    @Override
    public String toString() {
        //Opgave 2.1c
        StringBuilder builder = new StringBuilder();

        for (Plugin ingeladenPlugin : ingeladenPlugins) {


            if (ingeladenPlugin != null) {
                builder.append(" " + ingeladenPlugin.getCommand());
            }
        }

        return "Geinstalleerde Plugins:" + builder.toString();
    }

    public String getLog() {

        String l = log.toString();
        log = new StringBuilder();

        return l;
    }
}
