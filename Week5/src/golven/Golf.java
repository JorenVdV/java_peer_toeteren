package golven;

/**
 * Created by brent on 16-10-2016.
 */
public class Golf {

    private double amplitude;
    private double frequentie;

    public Golf() {
        this.amplitude = 1.0;
        this.frequentie = 1.0;
    }

    public void setAmplitude(double amplitude) {
        if (amplitude != 0) {
            this.amplitude = amplitude;
        }
    }

    public void setFrequentie(double frequentie) {
        if (frequentie != 0) {
            this.frequentie = frequentie;
        }
    }

    public double getYwaarde(double x) {
        return amplitude * Math.sin(frequentie * x);
    }

    @Override
    public String toString() {
        if (amplitude == 1.0) {
            return String.format("y = sin (%.1f x)",frequentie);
        }
        else if(frequentie == 1.0) {
            return String.format("y = %.1f",amplitude);
        }
        return String.format("y = %.1f sin (%.1f x)",amplitude,frequentie);
    }
}
