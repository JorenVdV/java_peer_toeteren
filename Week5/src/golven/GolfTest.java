package golven;

/**
 * Created by brent on 16-10-2016.
 */
public class GolfTest {
    public static void main(String[] args) {

        for (double i = 1; i <= 5; i+=0.5) {
            Golf golf = new Golf();
            golf.setFrequentie(2.0);
            golf.setAmplitude(i);
            System.out.println(golf.toString());
        }

        GolvenGrafiek g = new GolvenGrafiek(5);

        g.tekenGolven();
    }
}
