package golven;

import java.awt.*;
import java.util.Random;

/**
 * Created by brent on 16-10-2016.
 */
public class GolvenGrafiek {

    private int aantal;
    private Random random;

    public GolvenGrafiek(int aantal) {
        this.aantal = aantal;
        this.random = new Random();
    }

    public void tekenGolven() {

        GrafiekWindow window = new GrafiekWindow(10,6);

        for (int i = 0; i < aantal; i++) {
            Color kleur = new Color(random.nextInt(255),random.nextInt(255),random.nextInt(255));
            Golf golf = new Golf();
            golf.setAmplitude(random.nextDouble()*4);
            golf.setFrequentie(random.nextDouble()*4);

            for (double j = -5; j < 5; j+=.001) {
                window.tekenPunt(j,golf.getYwaarde(j),kleur);
            }

        }

        window.toon();
    }
}
