package versie_1;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by brent on 24-10-2016.
 */
public class ChatBot {

    private String naam;
    private Random random;
    private String[] antwoorden;

    public ChatBot(String naam) {
        this.naam = naam;
        this.random = new Random();
        this.antwoorden = new String[] {"Ok, probeer eerst al eens te herstarten",
                "Kan je het probleem eens herformuleren?",
                "En is dat al lang zo?",
                "Waarom heb je ons niet eerder gecontacteerd?",
                "Ik denk dat je dat zelf wel kan oplossen, niet?",
                "Die vraag heb ik nog niet vaak gehad!",
                "Sorry, ik was even bezig, kan je de vraag eens herhalen?",
                "Aha, gekend probleem, ik zoek het op en laat je iets weten.",
                "Sorry, daar heb ik niet direct een antwoord op. Wat nu?",
                "En dan?",
                "Dat zal wel, maar er zijn ergere dingen he.",
                "Hmm, die moet ik opzoeken. En wanneer deed dat zich juist voor?"};

    }

    @Override
    public String toString() {
        return String.format("Hallo, ik ben chatbot %s, stel me een vraag en ik geef je een oplossing!",this.naam);

    }

    public String antwoordOpVraag(String vraag) {

        if (vraag.toLowerCase().equals("stop")) {
            return String.format("Je hebt blijkbaar geen vragen meer. Ok, dan ben ik weg. De groeten van %s",this.naam);
        }

        if (vraag.length() < 4) {
            return "";
        }

        return this.antwoorden[random.nextInt(this.antwoorden.length-1)];
    }
}
