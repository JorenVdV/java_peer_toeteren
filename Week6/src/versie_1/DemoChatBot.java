package versie_1;

import java.util.Scanner;

/**
 * Created by brent on 24-10-2016.
 */
public class DemoChatBot {

    public static void main(String[] args) {
        ChatBot chatbot = new ChatBot("Veronica");
        System.out.println(chatbot);
        System.out.println("Tik \"stop\" om te eindigen");
        Scanner scanner = new Scanner(System.in);

        while(true) {
            System.out.println(chatbot.antwoordOpVraag(scanner.nextLine()));

        }
    }
}
