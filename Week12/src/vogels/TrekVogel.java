package vogels;

/**
 * PEER TUTORING
 * P2W6
 */

// gegeven, mag je wijzigen
public class TrekVogel extends Vogel{
    // hier aanvullen
    private final String winterbestemming;

    public TrekVogel(String naam, Habitat habitat, String winterbestemming) {
        super(naam, habitat);
        this.winterbestemming = winterbestemming;
    }

    @Override
    public boolean isTrekker() {
        return true;
    }

    @Override
    public String toString() {
        return String.format("%s --> trekt naar %s", super.toString(), winterbestemming);
    }
}
