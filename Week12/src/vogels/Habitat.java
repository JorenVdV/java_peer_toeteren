package vogels;

/**
 * PEER TUTORING
 * P2W6
 */

// gegeven, mag je wijzigen
public enum Habitat {
    // hier aanvullen
    BOS("bos"),
    WEIDE("weide"),
    TUIN("tuin"),
    ZOET_WATER("zoet water"),
    ZEE("zee"),
    STAD("stad"),
    MOERAS("moeras");

    private final String value;
    Habitat(String value){
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
