package vogels;

/**
 * PEER TUTORING
 * P2W6
 */

// gegeven, mag je wijzigen
public abstract class Vogel implements Comparable<Vogel>{

    // gegeven, mag je niet wijzigen
    private final String naam;
    private final Habitat habitat;

    // gegeven, mag je niet wijzigen
    public Vogel(String naam, Habitat habitat) {
        this.naam = naam;
        this.habitat = habitat;
    }

    // hier aanvullen
    @Override
    public int compareTo(Vogel vogel) {
        return this.naam.compareTo(vogel.naam);
    }

    public abstract boolean isTrekker();

    public String verblijftIn(){
        return habitat.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        // check super class in geval geobserveerde
        if (o == null || (getClass() != o.getClass()&& getClass() != o.getClass().getSuperclass())) return false;

        Vogel vogel = (Vogel) o;

        return naam != null ? naam.equals(vogel.naam) : vogel.naam == null;
    }

    @Override
    public int hashCode() {
        return naam != null ? naam.hashCode() : 0;
    }

    @Override
    public String toString() {
        return String.format("%-20s habitat:%-10s", naam, habitat);
    }

    public String getNaam() {
        return naam;
    }

    public Habitat getHabitat() {
        return habitat;
    }
}
