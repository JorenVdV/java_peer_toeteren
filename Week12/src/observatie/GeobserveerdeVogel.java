package observatie;

import vogels.Habitat;
import vogels.TrekVogel;
import vogels.Vogel;

/**
 * PEER TUTORING
 * P2W6
 */

// gegeven, mag je wijzigen
public class GeobserveerdeVogel extends TrekVogel implements Identificeerbaar{

    // gegeven mag je niet wijzigen
    private String ringId;
    private String ringLocatie;
    private String observatie;
    private Vogel vogel;

    // gegeven
    public GeobserveerdeVogel(String naam, Habitat habitat, String bestemming, String ringId, String ringLocatie) {
        super(naam,habitat,bestemming);
        this.ringId = ringId;
        this.ringLocatie = ringLocatie;
        this.observatie = "nihil";
    }
    // hier aanvullen
    @Override
    public String getRingInfo() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("ID: %-12s LOC: %-12s", ringId, ringLocatie));
        if(!observatie.equals("nihil"))
            stringBuilder.append(String.format("OBS:\"%s\"",
                    observatie.length()>20?observatie.substring(0,20)+"...":observatie));

        return stringBuilder.toString();
    }

    public String getObservatie() {
        return observatie;
    }

    public void setObservatie(String observatie) {
        this.observatie = observatie;
    }

    @Override
    public String toString() {
        return String.format("%-20s Ringinfo:%s", vogel.getNaam(), getRingInfo());
    }
}
