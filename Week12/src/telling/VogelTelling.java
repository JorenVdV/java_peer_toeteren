package telling;

import observatie.GeobserveerdeVogel;
import vogels.StandVogel;
import vogels.Vogel;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * PEER TUTORING
 * P2W6
 */

// gegeven, mag je wijzigen
public class VogelTelling {
    public class Overzicht{
        private final String line = "---------------------------------------------------------------";
        public void toonOverzicht(){
            System.out.println(String.format("Vogeltelling op %s te %s", tijdstip.toString(), locatie));
            System.out.println(String.format("Weertype: %s", weerType));
            System.out.println(line);
            for(Map.Entry<Vogel, Integer> entry:tellingMap.entrySet()){
                System.out.println(String.format("%-12s %d",entry.getKey().getNaam(),entry.getValue()));
            }
            System.out.println(line);
            System.out.println(String.format("Totaal: %d exemplaren, %d soorten", getAantalExemplaren(), getAantalVogelNamen()));
            System.out.println(String.format("Verhouding trekvogels: %2.2f%%", getVerhoudingTrekVogels()*100));
            System.out.println(line);
            System.out.println("Logboek Ringinfo:");
            System.out.println(logBoek);
        }
    }

    // gegeven mag je niet wijzigen
    private String locatie;
    private LocalDateTime tijdstip;
    private String weerType;
    private StringBuilder logBoek;
    // hier aanvullen
    private Map<Vogel, Integer> tellingMap;

    // gegeven mag je wijzigen
    public VogelTelling(String locatie, LocalDateTime tijdstip, String weerType) {
        this.locatie = locatie;
        this.tijdstip = tijdstip;
        this.weerType = weerType;
        this.logBoek = new StringBuilder();
        this.tellingMap = new HashMap<>();
    }

    // gegeven mag je wijzigen
    public void voegVogelToe(Vogel vogel, int aantal) {
        if(tellingMap.containsKey(vogel)){
            tellingMap.put(vogel,tellingMap.get(vogel)+aantal);
        }else
            tellingMap.putIfAbsent(vogel, aantal);
        if(vogel.getClass().equals(GeobserveerdeVogel.class))
            logBoek.append(String.format("%s \n", ((GeobserveerdeVogel)vogel).getRingInfo()));
    }

    // gegeven mag je wijzigen
    public int getAantalExemplaren() {
        /*
        int total = 0;
        for (Map.Entry<Vogel,Integer> entry:tellingMap.entrySet()) {
            total+=entry.getValue();
        }
        return total;
        */
        return tellingMap.values().stream().mapToInt(v->v).sum();
    }

    // gegeven mag je wijzigen
    public int getAantalVogelNamen() {
        /*
        Set<String> vogelnamen = new HashSet<>();
        for (Map.Entry<Vogel,Integer> entry:tellingMap.entrySet())
            vogelnamen.add(entry.getKey().getNaam());
        return vogelnamen.size();
        */
        return ((int) tellingMap.keySet().stream().map(v -> v.getNaam()).distinct().count());
    }

    // gegeven mag je wijzigen
    public double getVerhoudingTrekVogels() {
        double total = getAantalExemplaren();
        double totalTrek = tellingMap.entrySet().stream()
                .filter(entry -> entry.getKey().getClass()!= StandVogel.class)
                .mapToInt(entry -> entry.getValue())
                .sum();
        return totalTrek/total;
    }

    // hier aanvullen

}
